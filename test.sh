#!/bin/bash

if [ $# -eq 0 ]; then
    echo "Використання: $0 <шлях до каталогу>"
    exit 1
fi
if [ ! -d "$1" ]; then
    echo "Помилка: аргумент має бути каталогом"
    exit 1
fi
directory=$1
html_file="$directory/files.html"

cat > $html_file << EOF
<!DOCTYPE html>
<html lang="uk">
<head>
<meta charset="UTF-8">
<title>Файли в каталозі</title>
</head>
<body>
<h1>Файли в каталозі $directory</h1>
<table border="1">
<tr>
    <th>Ім'я файлу</th>
    <th>Розмір</th>
    <th>Дата зміни</th>
</tri>
EOF

for file in "$directory"/*; do
    if [ ! -d "$file" ]; then
        filename=$(basename "$file")
        size=$(stat -c%s "$file")
        mtime=$(date -r "$file" "+%d-%m-%Y %H:%M:%S")
        echo "<tr><td><a href=\"$filename\">$filename</a></td><td>$size</td><td>$mtime</td></tr>" >> $html_file
    fi
done
echo "</table>" >> $html_file
echo "</body>" >> $html_file
echo "</html>" >> $html_file
echo "Створено файл HTML: $html_file"
